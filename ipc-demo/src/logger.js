/**
 * Log a message to the console
 * @param {string} message
 * @param {string} level: default: 'Info'
 */
exports.log = function (message, level = 'Info') {
  console.log('%s %s: %s', new Date().toISOString(), level, message);
}

// Note: exports.log is the CommonJS equivalent of ES6+ export keyword
// use it in combination with require()
// https://requirejs.org/docs/commonjs.html