// Note: In Electron there are 2 types of Processes
// - Main Process:
//     Always starts first
//     There is only 1 Main-Process
//     Acts as Main-Controller for the app,
//     Manages the App-Lifecycle
//     Is headless and responsible for spawning Render-Processes
//     API: https://github.com/electron/electron/tree/master/docs#modules-for-the-main-process
// - Render Processes:
//     Usually a UI-window
//     There can be more than 1 Render-Processes
//     Can also be used for creating other Processes for concurrent work
//     API: https://github.com/electron/electron/tree/master/docs#modules-for-the-renderer-process-web-page

// Processes communicate with each other through IPC (Inter Process Communication)
//    a Button click in a Render-Process has to talk through IPC with the Controller/Main-Process

const logger = require('./logger');

const electron = require('electron');
const ipc = electron.ipcRenderer;

// You will see this console entry in the windows-/electron- console -> open Chrome-DeveloperTools
logger.log('Renderer.js called');

document.getElementById('start-button').addEventListener('click', () => {
  logger.log(`Render-Process: IPC-Event 'countdown-start' sent.`);

  // Send a new event with the identifier 'countdown-start'. Other Processes can listen for this event.
  ipc.send('countdown-start');
});


// Listen for countdown ticks send by other processes (Main.js)
ipc.on('countdown', (event, count) => {
  logger.log(`Render-Process: IPC-Event 'countdown' received with data '${count}'.`);
  document.getElementById('start-button').innerText = count !== 0 ? count : 'Start';
});