const electron = require('electron');
const logger = require('./logger');
const countdown = require('./countdown');
const utils = require('./utils');

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const windowCountToSpawn = 2;

app.on('ready', () => {
  logger.log('Electron is ready.')

  // note variable scope
  browserWindows = utils.range(windowCountToSpawn).map(i => {
    let window = openBrowserWindow(400, 400, `Window ${i}`);
    handleBrowserWindowClosing(window);
    loadHtmlFile(window, `file://${__dirname}/countdown.html`);

    return window;
  });
});


const ipc = electron.ipcMain;
ipc.on('countdown-start', () => {
  logger.log(`Main-Process: IPC-Event 'countdown-start' received.`);

  // On each tick, execute the specified functino -> send the current tick-count through IPC to the Renderers
  // webContents is an eventEmitter instance
  countdown(count => {
    logger.log(`Main-Process: IPC-Event 'countdown' sent with value '${count}'`);
    browserWindows.forEach(window => window.webContents.send('countdown', count));
  });
})


function loadHtmlFile(window, filePath) {
  logger.log(`Loading file '${filePath}'...`);

  // Beware, its URL not Url
  window.loadURL(filePath);
}


/**
 * @summary React to the window close event. Clean up ie. release it to the garbage collector
 * @param {BrowserWindow} window
 */
function handleBrowserWindowClosing(window) {
  window.on('closed', () => {
    logger.log(`Window '${window.title}' disposed.`);
    window = null;
  });
}

/**
 * @summary
 */
function openBrowserWindow(width, height, title) {
  logger.log('Opening new window.');

  const window = new BrowserWindow({
    width,
    height,
    title
  });
  return window;
}