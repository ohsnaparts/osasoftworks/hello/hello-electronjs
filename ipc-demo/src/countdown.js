const logger = require('./logger');

module.exports = function countdown(tickFn) {
  logger.log('Countdown started');
  let count = 10;

  var intervalHandler = setInterval(() => {
    logger.log(`  ${count}`);
    tickFn(count);

    if (count-- === 0)
      clearInterval(intervalHandler);
  }, 1000);
}