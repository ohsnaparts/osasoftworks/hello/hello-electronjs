exports.range = function (count) {
  return [...Array(count).keys()]
}